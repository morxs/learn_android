package com.rudsu.learningandroid2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
//import android.app.Activity;
//import android.view.Menu;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;

public class MainActivity extends SherlockActivity {
    public final static String EXTRA_MESSAGE = "com.rudsu.learningandroid2.MESSAGE";

    public final static int DARK_THEME = 0;
    public final static int LIGHT_THEME = 1;
    public final static int GREEN_THEME = 2;

    public static int currTheme = GREEN_THEME; // initialization
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (currTheme != GREEN_THEME) {
            changeTheme(currTheme);
        }
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void sendMessage(View view) {
        switch (view.getId()) {
            case R.id.button_send:
                Intent intent = new Intent(this, DisplayMessageActivity.class);
                EditText editText = (EditText) findViewById(R.id.edit_message);
                String message = editText.getText().toString();

                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // double press back to exit
        if (back_pressed + 2000 > System.currentTimeMillis())
            super.onBackPressed();
        else
            Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }

    private void changeTheme(int theme) {
        switch (theme) {
            case DARK_THEME:
                setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
                break;
            case LIGHT_THEME:
                setTheme(R.style.Theme_Sherlock_Light);
                break;
            case GREEN_THEME:
                setTheme(R.style.Theme_Mythemegreen);
                break;
        }
    }

    public void viewActivity(View view) {
        switch (view.getId()) {
            case R.id.button_light_theme:
                finish();
                currTheme = LIGHT_THEME;
                Intent recreateActivityWithLight = new Intent(this, MainActivity.class);
                startActivity(recreateActivityWithLight);
                break;
            case R.id.button_dark_theme:
                finish();
                currTheme = DARK_THEME;
                Intent recreateActivityWithDark = new Intent(this, MainActivity.class);
                startActivity(recreateActivityWithDark);
                break;
            case R.id.button_green_theme:
                finish();
                currTheme = GREEN_THEME;
                Intent recreateActivityWithGreen = new Intent(this, MainActivity.class);
                startActivity(recreateActivityWithGreen);
                break;
        }
    }
    
}
